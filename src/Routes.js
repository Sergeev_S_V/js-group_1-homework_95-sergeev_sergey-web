import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import AddNewEvent from "./components/Event/AddNewEvent";
import EventsList from "./containers/EventsList/EventsList";
import FriendsList from "./containers/FriendsList/FriendsList";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/sign-up" />
);

const Routes = ({user, history}) => (
  <Switch>
    <ProtectedRoute
      isAllowed={user}
      path="/add_new_event"
      exact
      render={() => <AddNewEvent user={user} history={history}/>}
    />
    <Route path="/sign-up" exact component={Register}/>
    <ProtectedRoute
      isAllowed={user}
      path="/"
      exact
      render={() => <EventsList user={user}/>}/>
    <ProtectedRoute
      isAllowed={user}
      path="/events"
      exact
      render={() => <EventsList user={user}/>}/>
    <ProtectedRoute
      isAllowed={user}
      path="/events/my_friends"
      exact
      render={() => <FriendsList user={user}/>}/>
  </Switch>
);

export default Routes;