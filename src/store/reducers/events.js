import {DELETE_EVENT_SUCCESS, FETCH_EVENTS_SUCCESS, SEND_EVENT_SUCCESS} from "../sagas/actionTypes";

const initialState = {
  events: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SEND_EVENT_SUCCESS:
      return {...state, events: state.events.concat(action.eventData)};
    case FETCH_EVENTS_SUCCESS:
      return {...state, events: action.events};
    case DELETE_EVENT_SUCCESS:
      return {...state, events: action.events};
    default:
      return state;
  }
};

export default reducer;