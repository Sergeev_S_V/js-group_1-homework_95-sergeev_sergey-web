import {ADD_FRIEND_SUCCESS, DELETE_FRIEND_SUCCESS, FETCH_FRIENDS_SUCCESS} from "../sagas/actionTypes";

const initialState = {
  friends: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FRIENDS_SUCCESS:
      return {...state, friends: action.friends};
    case ADD_FRIEND_SUCCESS:
      return {...state, friends: state.friends.concat(action.friend)};
    case DELETE_FRIEND_SUCCESS:
      return {...state, friends: action.friends};
    default:
      return state;
  }
};

export default reducer;