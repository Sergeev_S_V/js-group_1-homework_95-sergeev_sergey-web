import axios from "../../axios-api";
import {put} from "redux-saga/effects";
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";

import {
  FACEBOOK_LOGIN_FAILURE, FACEBOOK_LOGIN_SUCCESS, FACEBOOK_LOGIN_USER, LOGOUT_EXPIRED_USER, LOGOUT_EXPIRED_USER_SUCCESS,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS
} from "./actionTypes";

export function* facebookLoginSaga(action) {
  try {
    const response = yield axios.post('/users/facebookLogin', action.data);
    yield put(loginUserSuccess(response.data.user, response.data.token));
    yield put(push('/'));
    yield NotificationManager.success('Logged in with Facebook!', 'Success');
  } catch (error) {
    yield put(loginUserFailure(error.response.data));
  }
}

export const facebookLogin = data => {
  return {type: FACEBOOK_LOGIN_USER, data}
};

const loginUserSuccess = (user, token) => {
  return {type: FACEBOOK_LOGIN_SUCCESS, user, token};
};

const loginUserFailure = error => {
  return {type: FACEBOOK_LOGIN_FAILURE, error};
};

export const logoutUser = () => ({
  type: LOGOUT_USER
});

export function* logoutUserSaga() {
  const response = yield axios.delete('/users/sessions');
  yield put(push('/sign-up'));
  yield put(logoutUserSuccess());
  yield NotificationManager.success('Success', response.data.message);
}

const logoutUserSuccess = () => ({
  type: LOGOUT_USER_SUCCESS
});

export const logoutExpiredUser = () => ({
  type: LOGOUT_EXPIRED_USER
});

export function* logoutExpiredUserSaga() {
    yield put(push('/sign-up'));
    yield put(logoutUserSuccess());
    yield NotificationManager.error('Error', 'Your session has expired, please login again');
}