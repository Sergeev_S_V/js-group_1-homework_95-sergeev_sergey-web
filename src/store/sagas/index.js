import {takeEvery} from "redux-saga/effects";
import {FACEBOOK_LOGIN_USER} from "../sagas/actionTypes";
import {facebookLoginSaga, logoutExpiredUserSaga, logoutUserSaga} from "./users";
import {deleteEventSaga, fetchEventsSaga, sendEventSaga} from "./events";
import {
  ADD_FRIEND, DELETE_EVENT, DELETE_FRIEND, FETCH_EVENTS, FETCH_FRIENDS, LOGOUT_EXPIRED_USER, LOGOUT_USER,
  SEND_EVENT
} from "./actionTypes";
import {addFriendSaga, deleteFriendSaga, fetchFriendsSaga} from "./friends";

export function* watchFacebookLoginUser() {
  yield takeEvery(FACEBOOK_LOGIN_USER, facebookLoginSaga);
}

export function* watchLogoutUser() {
  yield takeEvery(LOGOUT_USER, logoutUserSaga);
}

export function* watchLogoutExpiredUser() {
  yield takeEvery(LOGOUT_EXPIRED_USER, logoutExpiredUserSaga);
}

export function* watchSendEvent() {
  yield takeEvery(SEND_EVENT, sendEventSaga);
}

export function* watchFetchEvents() {
  yield takeEvery(FETCH_EVENTS, fetchEventsSaga);
}

export function* watchDeleteEvent() {
  yield takeEvery(DELETE_EVENT, deleteEventSaga);
}

export function* watchFetchFriends() {
  yield takeEvery(FETCH_FRIENDS, fetchFriendsSaga);
}

export function* watchAddFriend() {
  yield takeEvery(ADD_FRIEND, addFriendSaga);
}

export function* watchDeleteFriend() {
  yield takeEvery(DELETE_FRIEND, deleteFriendSaga);
}



