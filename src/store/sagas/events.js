import axios from "../../axios-api";
import {put} from "redux-saga/effects";
import {push} from "react-router-redux";
import {
  DELETE_EVENT, DELETE_EVENT_SUCCESS, FETCH_EVENTS, FETCH_EVENTS_SUCCESS, SEND_EVENT,
  SEND_EVENT_SUCCESS
} from "./actionTypes";

// send event

export const sendEvent = eventData => {
  return {type: SEND_EVENT, eventData}
};

export function* sendEventSaga(action) {
  try {
    const response = yield axios.post('/events', action.eventData);
    yield put(sendEventSuccess(response.data));
    yield put(push('/'));
  } catch (error) {
    // yield put(loginUserFailure(error.response.data));
  }
}

const sendEventSuccess = eventData => {
  return {type: SEND_EVENT_SUCCESS, eventData};
};

// fetch events

export const fetchEvents = () => {
  return {type: FETCH_EVENTS}
};

export function* fetchEventsSaga() {
  try {
    const response = yield axios.get('/events');
    yield put(fetchEventsSuccess(response.data));
  } catch (error) {
    // yield put(loginUserFailure(error.response.data));
  }
}

const fetchEventsSuccess = events => {
  return {type: FETCH_EVENTS_SUCCESS, events};
};

// delete event

export const deleteEvent = id => {
  return {type: DELETE_EVENT, id};
};

export function* deleteEventSaga(action) {
  try {
    const response = yield axios.delete(`/events/${action.id}`);
    yield put(deleteEventSuccess(response.data.events));
  } catch (err) {

  }
}

const deleteEventSuccess = events => {
  return {type: DELETE_EVENT_SUCCESS, events};
};




