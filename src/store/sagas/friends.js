import {
  ADD_FRIEND, ADD_FRIEND_SUCCESS, DELETE_FRIEND, DELETE_FRIEND_SUCCESS, FETCH_FRIENDS,
  FETCH_FRIENDS_SUCCESS
} from "./actionTypes";
import axios from "../../axios-api";
import {put} from "redux-saga/effects";

// fetch friends
export const fetchFriends = () => {
  return {type: FETCH_FRIENDS};
};

export function* fetchFriendsSaga() {
  try {
    const response = yield axios.get(`/friends`);
    yield put(fetchFriendsSuccess(response.data));
  } catch (err) {

  }
}

const fetchFriendsSuccess = friends => {
  return {type: FETCH_FRIENDS_SUCCESS, friends};
};

// add friends
export const addFriend = email => {
  return {type: ADD_FRIEND, email}
};

export function* addFriendSaga(action) {
  try {
    const response = yield axios.put(`/friends?email=${action.email}`);
    yield put(addFriendSuccess(response.data.friends));
  } catch (err) {

  }
}

const addFriendSuccess = friend => ({
  type: ADD_FRIEND_SUCCESS, friend
});

// delete friend

export const deleteFriend = id => {
  return {type: DELETE_FRIEND, id};
};

export function* deleteFriendSaga(action) {
  try {
    const response = yield axios.delete(`/friends/${action.id}`);
    yield put(deleteFriendSuccess(response.data.friends));
  } catch (err) {

  }
}

const deleteFriendSuccess = friends => {
  return {type: DELETE_FRIEND_SUCCESS, friends};
};