import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {routerMiddleware, routerReducer} from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import createSagaMiddleware from 'redux-saga';

import usersReducer from "./reducers/users";
import eventsReducer from "./reducers/events";
import friendsReducer from "./reducers/friends";
import {saveState, loadState} from "./localStorage";
import {
  watchAddFriend, watchDeleteEvent, watchDeleteFriend, watchFacebookLoginUser, watchFetchEvents,
  watchFetchFriends, watchLogoutExpiredUser, watchLogoutUser,
  watchSendEvent
} from './sagas';

const rootReducer = combineReducers({
  users: usersReducer,
  events: eventsReducer,
  friends: friendsReducer,
  routing: routerReducer
});

export const history = createHistory();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  routerMiddleware(history),
  sagaMiddleware
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

sagaMiddleware.run(watchFacebookLoginUser);
sagaMiddleware.run(watchLogoutUser);
sagaMiddleware.run(watchLogoutExpiredUser);
sagaMiddleware.run(watchSendEvent);
sagaMiddleware.run(watchFetchEvents);
sagaMiddleware.run(watchDeleteEvent);
sagaMiddleware.run(watchFetchFriends);
sagaMiddleware.run(watchAddFriend);
sagaMiddleware.run(watchDeleteFriend);

store.subscribe(() => {
  saveState({
    users: store.getState().users
  });
});

export default store;