import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteFriend, fetchFriends} from "../../store/sagas/friends";
import {Button, PageHeader, Panel} from "react-bootstrap";

class FriendsList extends Component {

  componentDidMount() {
    this.props.onFetchFriends();
  };

  render() {
    return(
      <Fragment>
        <PageHeader>Friends list</PageHeader>
        {this.props.friendsList.length > 0
          && this.props.friendsList.map(friend => (
            <Panel key={friend._id}>
              <Panel.Heading>
                <span>
                  {friend.displayName}
                </span>
                <Button onClick={() => this.props.onDeleteFriend(friend._id)}>
                  Delete
                </Button>
              </Panel.Heading>
            </Panel>
          ))
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  friendsList: state.friends.friends
});

const mapDispatchToProps = dispatch => ({
  onFetchFriends: () => dispatch(fetchFriends()),
  onDeleteFriend: id => dispatch(deleteFriend(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(FriendsList);