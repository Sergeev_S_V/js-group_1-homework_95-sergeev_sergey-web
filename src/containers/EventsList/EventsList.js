import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteEvent, fetchEvents} from "../../store/sagas/events";
import {Button, Col, Panel} from "react-bootstrap";

class EventsList extends Component {

  componentDidMount() {
    this.props.onFetchEvents();
  };

  render() {
    return(
      <Fragment>
        <Col md={12}>
          {this.props.events.length > 0
            && this.props.events.map(event => (
                <Panel key={event._id}>
                  <Panel.Heading>
                    <span><b>Start datetime: {event.startDateTime} </b></span>
                    <span><b>End datetime: {event.endDateTime} </b></span>
                    <Button onClick={() => this.props.onDeleteEvent(event._id)}>
                      Delete
                    </Button>
                  </Panel.Heading>
                  <Panel.Body>
                    {event.title}
                  </Panel.Body>
                </Panel>
              ))
          }
        </Col>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  events: state.events.events
});

const mapDispatchToProps = dispatch => ({
  onFetchEvents: () => dispatch(fetchEvents()),
  onDeleteEvent: id => dispatch(deleteEvent(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EventsList);