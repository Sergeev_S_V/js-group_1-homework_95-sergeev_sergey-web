import React, {Component, Fragment} from 'react';
import {Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FacebookLogin from "../../components/Auth/FacebookLogin/FacebookLogin";

class Register extends Component {

  render() {
    return (
      <Fragment>
        <PageHeader>Register and login</PageHeader>
        <Form horizontal>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <FacebookLogin />
            </Col>
          </FormGroup>

        </Form>
      </Fragment>
    )
  }
}

export default Register;