import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import './Layout.css';
import Toolbar from "../../components/UI/Toolbar/Toolbar";

const Layout = props => (
  <Fragment>
    <NotificationContainer />
    <header>
      <Toolbar user={props.user}/>
    </header>
    <main className="container">
      {props.children}
    </main>
  </Fragment>
);

const mapStateToProps = state => ({
  user: state.users.user
});

export default connect(mapStateToProps)(Layout);