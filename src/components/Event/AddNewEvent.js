import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";
import {connect} from "react-redux";
import {sendEvent} from "../../store/sagas/events";

class AddNewEvent extends Component {

  state = {
    title: '',
    startDateTime: '',
    endDateTime: ''
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  sendEventHandler = event => {
    event.preventDefault();

    this.props.onSendEvent(this.state);
  };

  render() {
    return(
      <Fragment>
        <PageHeader>Add new event</PageHeader>
        <Form horizontal onSubmit={this.sendEventHandler}>
          <FormElement required
                       propertyName='title'
                       title='Title'
                       placeholder='Enter title'
                       type='text'
                       autoComplete='current-title'
                       value={this.state.title}
                       changeHandler={this.inputChangeHandler}
          />

          <FormElement required
                       propertyName='startDateTime'
                       title='Start datetime'
                       placeholder='Enter start datetime'
                       type='date'
                       autoComplete='current-startDatetime'
                       value={this.state.startDateTime}
                       changeHandler={this.inputChangeHandler}
          />

          <FormElement required
                       propertyName='endDateTime'
                       title='End datetime'
                       placeholder='Enter end datetime'
                       type='date'
                       autoComplete='current-endDatetime'
                       value={this.state.endDateTime}
                       changeHandler={this.inputChangeHandler}
          />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">Add</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onSendEvent: eventData => dispatch(sendEvent(eventData))
});

export default connect(null, mapDispatchToProps)(AddNewEvent);