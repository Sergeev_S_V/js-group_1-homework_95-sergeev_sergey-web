import React, {Component} from 'react';
import {Button, FormControl, FormGroup, Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";
import {connect} from "react-redux";
import {addFriend} from "../../../store/sagas/friends";
import {logoutUser} from "../../../store/sagas/users";

class Toolbar extends Component {

  state = {
    email: ''
  };

  onChangeInputHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  render() {
    return(
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <LinkContainer to="/" exact><a>Events calendar</a></LinkContainer>
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to="/events" exact>
              <NavItem>Events</NavItem>
            </LinkContainer>
          </Nav>
          {this.props.user &&
            <Navbar.Form pullLeft>
              <FormGroup>
                <FormControl required
                             type="email"
                             name='email'
                             placeholder="Add friend"
                             onChange={this.onChangeInputHandler}
                             value={this.state.email}
                />
              </FormGroup>
              <Button onClick={() => this.props.onAddFriend(this.state.email)}
                      type="submit">Add</Button>
            </Navbar.Form>
          }
          {this.props.user
            ? <UserMenu user={this.props.user} logout={this.props.logoutUser}/>
            : <AnonymousMenu/>
          }
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onAddFriend: email => dispatch(addFriend(email)),
  logoutUser: () => dispatch(logoutUser())
});

export default connect(null, mapDispatchToProps)(Toolbar);