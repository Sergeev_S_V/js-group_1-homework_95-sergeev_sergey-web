import React, {Fragment} from 'react';
import {MenuItem, Nav, NavDropdown} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserMenu = ({user, logout}) => {
  const navTitle = (
    <Fragment>
      Hello, <b>{user.displayName}</b>!
    </Fragment>
  );

  return (
    <Nav pullRight>
      <NavDropdown title={navTitle} id="user-menu">
        <LinkContainer to="/add_new_event">
          <MenuItem>Add event</MenuItem>
        </LinkContainer>
        <MenuItem divider/>
        <LinkContainer to="/events/my_friends">
          <MenuItem>Friends list</MenuItem>
        </LinkContainer>
        <MenuItem onClick={logout}>Logout</MenuItem>
      </NavDropdown>
    </Nav>
  )
};

export default UserMenu;