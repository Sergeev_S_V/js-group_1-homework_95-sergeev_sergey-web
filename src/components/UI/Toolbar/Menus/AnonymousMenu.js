import React from 'react';
import {Nav, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const AnonymousMenu = () => (
  <Nav pullRight>
    <LinkContainer to="/sign-up" exact>
      <NavItem>Sign Up</NavItem>
    </LinkContainer>
  </Nav>
);

export default AnonymousMenu;